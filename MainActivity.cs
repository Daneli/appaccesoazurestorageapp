﻿using Android.App;
using Android.OS;
using Android.Runtime;
using AndroidX.AppCompat.App;
using Android.Widget;
using Plugin.Media;
using System;
using System.IO;
using Plugin.CurrentActivity;
using Android.Graphics;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace AccesoAzureStorageApp
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        string Archivo;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            SupportActionBar.Hide();
            CrossCurrentActivity.Current.Init(this, savedInstanceState);
            var Imagen = FindViewById<ImageView>(Resource.Id.imagen);
            var btnAlmacenar = FindViewById<Button>(Resource.Id.btnalmacenar);
            var txtNombre = FindViewById<EditText>(Resource.Id.txtnombre);
            var txtCarrera = FindViewById<EditText>(Resource.Id.txtcarrera);
            var txtSemestre = FindViewById<EditText>(Resource.Id.txtsemestre);
            var txtSaldo = FindViewById<EditText>(Resource.Id.txtsaldo);
            Imagen.Click += async delegate
            {
                await CrossMedia.Current.Initialize();
                var archivo = await CrossMedia.Current.TakePhotoAsync
                (new Plugin.Media.Abstractions.StoreCameraMediaOptions
                {
                    Directory = "Imagenes",
                    Name = txtNombre.Text,
                    SaveToAlbum = true,
                    CompressionQuality = 30,
                    PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,
                    DefaultCamera = Plugin.Media.Abstractions.CameraDevice.Front
                });
                if (archivo == null)
                    return;
                    Bitmap bp = BitmapFactory.DecodeStream(archivo.GetStream());
                    Archivo = System.IO.Path.Combine(System.Environment.GetFolderPath
                        (System.Environment.SpecialFolder.Personal), txtNombre.Text + ".jpg");
                    var stream = new FileStream(Archivo, FileMode.Create);
                    bp.Compress(Bitmap.CompressFormat.Jpeg, 30, stream);
                    stream.Close();
                    Imagen.SetImageBitmap(bp);
                
            };
            btnAlmacenar.Click += async delegate
            {
                try
                {
                    var CuantaAlmacenamiento = CloudStorageAccount.Parse
                    ("DefaultEndpointsProtocol=https;AccountName=danelireyes;AccountKey=f/erjBfcReYACyddwlEWV+BxHrP2PgUKalKlKs2p1bJRzJREM0LgjtLuCVitNiIrU66vU08IwrHDJsBNwSm+7A==;EndpointSuffix=core.windows.net"); 
                    
                    var ClienteBlob = CuantaAlmacenamiento.CreateCloudBlobClient();
                    var Carpeta = ClienteBlob.GetContainerReference("imagenes");
                    var resourseBlob = Carpeta.GetBlockBlobReference(txtNombre.Text + ".jpg");
                    resourseBlob.Properties.ContentType = "image/jpeg";
                    await resourseBlob.UploadFromFileAsync(Archivo.ToString());
                   
                    Toast.MakeText(this, "Almacenado en contenedor de Azure", ToastLength.Long).Show();
                   
                    var TablaNoSQL = CuantaAlmacenamiento.CreateCloudTableClient();
                    var Coleccion = TablaNoSQL.GetTableReference("alumnos");
                    
                    await Coleccion.CreateIfNotExistsAsync();
                    var alumno = new Alumnos("Alumnos", txtNombre.Text);
                    alumno.Carrera = txtCarrera.Text;
                    alumno.Semestre = txtSemestre.Text;
                    alumno.Saldo = double.Parse(txtSaldo.Text);

                    alumno.imagen = "https://danelireyes.blob.core.windows.net/imagenes/" + txtNombre.Text + ".jpg";
                    var Store = TableOperation.Insert(alumno);
                    await Coleccion.ExecuteAsync(Store);
                    Toast.MakeText(this, "Datos almacenados en Tabla NoSQL", ToastLength.Long).Show();
                
                }
                catch (Exception ex)
                {

                    Toast.MakeText(this, ex.Message, ToastLength.Long).Show();
                }
            };
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public class Alumnos : TableEntity
    {
        public Alumnos(string Categoria, string Nombre)
        {
            PartitionKey = Categoria;
            RowKey = Nombre;
        }
        public string Carrera { get; set; }
        public string Semestre { get; set; }
        public double Saldo { get; set; }
        public string imagen { get; set; }

    }
}